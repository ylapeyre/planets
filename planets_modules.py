"""
the modulesss
"""

import numpy as np

sun_mass = 0.0002


def velocity_verlet(planet_coord, planet_vel, sun_mass):
    """
    Computes the velocity of the planet using the velocity Verlet method.

    Parameters
    ----------
    planet_coord : numpy array
        x, y coordinates of the planet.
    planet_vel : numpy array
        vx vy velocities.
    sun_mass : float
        mass of the sun.

    Returns
    -------
    planet_coord : numpy array
        x, y coordinates of the planet.
    planet_vel : numpy array
        vx vy velocities.

    """
    planet_sun_distance = Planet_sun_distance(planet_coord)
    planet_sun_direction = Planet_sun_direction(planet_coord, planet_sun_distance)
    acceleration_t = Acceleration(
        planet_coord, planet_sun_distance, planet_sun_direction
    )

    # update with acceleration at t
    planet_coord += planet_vel + 0.5 * acceleration_t

    planet_sun_distance = Planet_sun_distance(planet_coord)
    planet_sun_direction = Planet_sun_direction(planet_coord, planet_sun_distance)
    acceleration_tp1 = Acceleration(
        planet_coord, planet_sun_distance, planet_sun_direction
    )

    # update with acceleration at t and t+1
    planet_vel += 0.5 * (acceleration_t + acceleration_tp1)
    return planet_coord, planet_vel


def Planet_sun_distance(planet_coord):
    """computes the distance between the sun and the planet"""
    return np.sqrt(np.sum(np.square(planet_coord)))


def Planet_sun_direction(planet_coord, planet_sun_distance):
    """computes the direction"""
    return -planet_coord / planet_sun_distance


def Acceleration(planet_coord, planet_sun_distance, planet_sun_direction):
    """computes the acceleration"""
    return (sun_mass / planet_sun_distance**2) * planet_sun_direction


def Initialize():
    pass
