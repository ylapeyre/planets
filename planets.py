import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import os

os.chdir("C:\\Users\\yona\\planets")
from planets_modules import velocity_verlet

sun_mass=0.0002
#setup the figure
fig, ax = plt.subplots()
ax.set_xlim(-1,1)
ax.set_ylim(-1,1)
ax.set_aspect('equal')

ax.plot([0],[0],'o',ms=30, c='gold')

#initial coords and vels
planet_coord = np.asarray([0.7,0.0])
planet_vel  = np.asarray([0,0.01])
c = np.random.choice(['blue', 'red', 'orange', 'cyan'])

N_planets = 1

line, =ax.plot([],[],color=c, marker = ".", ms=20)

def animate(i,planet_coord,planet_vel,sun_mass):

    planet_coord, planet_vel = velocity_verlet(planet_coord,planet_vel, sun_mass)
        
    line.set_data(planet_coord[0],planet_coord[1])  # update the data
    return line,

ani = animation.FuncAnimation(fig, animate, 
                              fargs=(planet_coord,planet_vel,sun_mass),
                              interval=2, 
                              blit=True,
                              )

def planet():
    #initial coords and vels
    planet_coord = np.asarray([np.random.random(), np.random.random()])
    planet_vel  = np.asarray([np.random.random(), np.random.random()])
    c = np.random.choice(['blue', 'red', 'orange', 'cyan'])
    
    line, =ax.plot([],[],color=c, marker = ".", ms=20)
    
    return planet_coord, planet_vel, line